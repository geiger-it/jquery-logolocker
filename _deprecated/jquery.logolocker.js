(function($) {

    $.fn.logolocker = function(options) {

        var logolocker_server='//logolocker.geiger.com';  // LIVE
        //var logolocker_server='//logolocker.app';  // Local

        var settings=$.extend(true,{
            host:logolocker_server,
            applicationSource:'GEIGER_STORE',
            action:'get_files',
            readonly:false,
            selected:'',  // this is the url to the currently selected image
            strictFileName:false,
            selectedClass:'active',
            canSelectItem:true,
            selectOnUpload:true,
            replaceSelectedOnUpload:false,
            hasActions:true,
            hasInfoAction:true,
            hasDownloadAction:true,
            hasRemoveAction:true,
            hasEmailAction:true,
            hasBootstrap:true,
            hideWhen:-1,
            template:'<div class="element" data-id="[id]"><a class="[active]"><img src="[imgsrc]" title="[filename]" data-location="[linknoquery]"></a>[actions]</div>', // DEPRECATED
            templates:{
                content:'<div class="ll_scroll_area" data-ll="locker-content">[items]</div>',
                item:'<div class="element" data-ll="locker-item"><a class="[selectedclass]"><img src="[imgsrc]" title="[filename]" data-location="[linknoquery]"></a>[actions]</div>',
                actions:'<div class="ll_action_wrap"><i class="fa fa-caret-left"></i></div><ul class="ll_actions">[actionlist]</ul>',
                progress:'<div class="ll_progress_gauge" data-ll="upload-progress"></div>',
                uploader:'<div class="ll_browse_bar row"><div class="col-md-6">[fileinput]</div><div class="col-md-6 text-right"><button type="submit" class="btn btn-default btn-xs browse_submit">Upload</button></div></div>'
            },
            logoMaxHeight:'90',
            logoMaxWidth:'180',
            noFiles:'No files are in this locker.',
            browsePlaceholderText:'',
            targetEl: $('.locked-on-target'),
            after: function() {}, // DEPRECATED -good
            afterLoaded: function() {},
            afterSelect: function() {},
            afterUpload: function() {}
        },options);

        if(options.template) {
            settings=$.extend(true,settings,{
                templates: {
                    item:options.template
                }
            },options);
        }

        if(settings.hasBootstrap) {
            settings=$.extend(true,settings,{
                selectedClass:'btn-success active',
                templates: {
                    content: '<div class="btn-toolbar form-group" data-ll="locker-content">[items]</div>',
                    item:'<div class="btn-group" data-ll="locker-item"><a href="#[filename]" class="btn btn-default [selectedclass]"><img src="[imgsrc]" title="[filename]" data-location="[linknoquery]" data-id="[id]"></a>[actions]</div>',
                    actions:'<button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right">[actionlist]</ul>',
                    progress:'<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" data-ll="upload-progress"></div></div>',
                    uploader:'<div class="input-group"><label class="input-group-btn"><span class="btn btn-default btn-file">Browse&hellip; [fileinput]</span></label><input type="text" class="form-control" readonly placeholder="'+settings.browsePlaceholderText+'"><span class="input-group-btn"><button type="submit" class="btn btn-info btn-file">Upload</button></span></div>'
                },
                logoMaxHeight:'48',
                logoMaxWidth:'96'
            },options);
        }

        /* Create a trigger for 'fileselect' */
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this);
            var numFiles = input.get(0).files ? input.get(0).files.length : 1;
            var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        function replaceBracketCode(html,code,value) {
            var regex=new RegExp("\\["+code+"\\]","gi");
            return html.replace(regex,value);
        }

        function process_template(template,params) {
            for(param in params) {
                template=replaceBracketCode(template,param,params[param]);
            }
            return template;
        }

        function strict_filename(theFilename) {
            theFilename=theFilename.replace(/^\./,'');
            var parts=theFilename.split('.');
            if(parts.length>1) {
                var extension='.'+parts.pop();
            } else {
                var extension='';
            }
            var filename=parts.join('.');
            // replace & with _and_, replace everything not A-Za-z0-9_ with _, replace multi _ with single _
            var cleaned_filename=filename.replace(/&/gi,'_and_').replace(/[^A-Za-z0-9_-]/gi,'_').replace(/_+/g,'_').replace(/_-_/g,'-');
            return cleaned_filename+extension;
        }

        function acceptable_filename(theFilename) {
            var offending_characters=[];
            var offending_characters_text=['&','+','(',')','#'];
            var suggested_filename=theFilename;
            var info={
                valid:true,
                filename:theFilename,
                offending_characters:offending_characters,
                offending_characters_text:offending_characters_text,
                suggested_filename:suggested_filename
            }
            for (var i=0; i<offending_characters_text.length; i++){
                var badChar = offending_characters_text[i];
                var position = info.suggested_filename.indexOf(badChar);
                if(position != -1){
                    info.offending_characters.push(info.suggested_filename.charAt(position));
                    info.suggested_filename = info.suggested_filename.substring(0,position) + info.suggested_filename.substring(position+1, info.suggested_filename.length);
                    info.valid = false;
                }
            }
            return info;
        }

        function delete_artwork(thumbnail,file_location) {
            if(confirm('Delete this art?')){
                var item_to_delete=thumbnail;
                var file_to_delete=file_location;
                var theURL=settings.host+'/file/delete';

                //console.log(theURL);
                //console.log(file_to_delete);
                //console.log(item_to_delete);

                //alert('DELETE '+file_to_delete);

                var formData = new FormData();
                formData.append('file_to_delete',file_to_delete);

                var xhr = new XMLHttpRequest();
                xhr.open('POST',theURL,true);
                xhr.onload=function () {
                    if (xhr.status === 200) {
                        var respText=xhr.responseText;
                        $('#errorDiv').html(respText);
                        //console.log(xhr.responseText);
                        thumbnail.remove();
                    } else {
                        var respText=xhr.responseText;
                        $('#errorDiv').html(respText);
                        alert('An error occurred!');
                    }
                };
                xhr.send(formData);
            }
        }

        function applyLogoMaxDimensions(locker) {
            var items=locker.find('[data-ll="locker-item"],[data-ll="locker-item-add"]');
            var maxHeight=parseInt(settings.logoMaxHeight);
            var maxWidth=parseInt(settings.logoMaxWidth);
            $.each(items, function(idx,item) {
                var btn=$(item).children('a,.btn');
                var img=$(btn).children('img');
                btn.css({lineHeight:(maxHeight+2)+'px'});
                img.css({maxHeight:maxHeight+'px',maxWidth:maxWidth+'px'});
            });
        }

        function render_locker(locker,filelist) {
            var params=[];
            var contentItems='';
            var file_count=filelist.length;
            locker.fileCount=file_count;
            locker.fileList=filelist;
            locker.activeFile = settings.selected;

            var contents=new Array;

            var html='';

            if(file_count<=settings.hideWhen) {
                $(locker.hide());
                return;
            }

            if(file_count==0) {
                html+=settings.noFiles;
            } else {
                $.each(filelist, function(index,object) {
                    // console.log(object);
                    params['url']=object.url;
                    params['link']=object.url;
                    params['linknoquery']=object.url.split("?")[0];
                    params['imgsrc']=object.imgsrc;
                    params['filename']=object.filename;
                    params['displayfilename']=object.display_filename;
                    params['id']=object.details.id;
                    params['size']=object.details.uploaded_size;
                    params['location']=object.location;
                    params['infolink']=settings.host+'/file/view?location='+encodeURIComponent(object.location);
                    params['deletelink']=settings.host+'/file/delete?location='+object.location;
                    if(params['linknoquery']==settings.selected) {
                        params['active']='active'; // DEPRECATED
                        params['selectedclass']=settings.selectedClass;
                    } else {
                        params['active']=''; // DEPRECATED
                        params['selectedclass']='';
                    }
                    contentItems+=process_template(settings.templates.item,params);
                    contents.push(object.location);
                });
            }

            var fileinputHtml='<input type="file" data-ll="fileinput" name="uploaded_file[]" multiple>';

            if(!settings.readonly && !settings.templates.uploader) {
                var inlineUploaderBrowseHtml='<div class="element" data-ll="locker-item-add"><a class="btn-file"><i class="plus-icon-thin"></i>'+fileinputHtml+'</a></div>';
                if(settings.hasBootstrap) {
                    inlineUploaderBrowseHtml='<div class="btn-group" data-ll="locker-item-add"><button type="button" class="btn btn-default btn-file"><i class="plus-icon-thin"></i>'+fileinputHtml+'</button></div>';
                }
                contentItems+=inlineUploaderBrowseHtml;
            }

            var contentParams={'items':contentItems};
            html+=process_template(settings.templates.content,contentParams);

            if(!settings.readonly) {
                if(settings.templates.uploader.length > 0) {
                    // html+=browse_bar;
                    var uploaderParams={
                        'fileinput':fileinputHtml
                    };
                    html+='<div class="ll_browse_bar_wrap">'+process_template(settings.templates.uploader,uploaderParams)+'</div>';
                }
                html+='<div class="ll_progress_bar_wrap">'+settings.templates.progress+'</div>';
            }
            //console.log(html);
            $(locker).html(html);

            locker.activePreview = locker.find('img[data-location="'+settings.selected+'"]').attr('src');
            applyLogoMaxDimensions(locker);
            settings.afterLoaded.call(this, locker);
            return contents;
        }

        var actions='';
        if(!settings.readonly) {
            if(settings.hasActions) {
                var actionList='';
                if(settings.hasInfoAction) {
                    actionList+='<li><a href="[infolink]" data-action="info" target="_blank">INFO</a></li>';
                }
                if(settings.hasDownloadAction) {
                    actionList+='<li><a href="[link]" data-action="download" target="_blank">DOWNLOAD</a></li>';
                }
                if(settings.hasRemoveAction) {
                    actionList+='<li><a href="[deletelink]" data-action="remove" target="_blank">REMOVE</a></li>';
                }
                if(settings.hasEmailAction) {
                    actionList+='<li><a href="mailto:?subject=[filename]&body=Link%20to%20[filename]:%20[linknoquery]" data-action="email" target="_blank">E-MAIL</a></li>';
                }
                actions=replaceBracketCode(settings.templates.actions,'actionlist',actionList);
            }
        }
        
        // add actions to the template if [actions] tag is present
        settings.templates.item=replaceBracketCode(settings.templates.item,'actions',actions);

        if(!settings.path){
            console.error("Locker Path Undefined. Format: GeigerGroup/Geiger/SalesRep/{email@domain.com}/branding");
        }
                
        $('body').on('mouseover','.ll_action_wrap',function(e) {
            //console.log('mouse enter action');
            //console.log($(this).next('.ll_actions'));
            $(this).hide();
            $(this).next('.ll_actions').show();
        });
        $('body').on('mouseleave','[data-ll="locker-item"]',function(e) {
            //console.log('mouse leave action');
            //console.log($(this).prev('.ll_action_wrap'));
            //$(this).hide();
            //$(this).prev('.ll_action_wrap').show();
            $('.ll_action_wrap').show();
            $('.ll_actions').hide();
        });

        this.each( function() {
            var locker=$(this);

            locker.activeFile = '';
            locker.activePreview = '';
            locker.fileCount = 0;
            locker.fileList = {};
            locker.autoselectitem='';

            function load_files() {
                var url=settings.host+'/bin?path='+data_path;

                //console.log(url);
                //console.log('LOAD-GET');

                var xhr = new XMLHttpRequest();

                xhr.onload=function() {
                    if (xhr.status === 200) {
                        var respText=xhr.responseText;
                        var file_list=$.parseJSON(respText);
                        //console.log('FILELIST');
                        //console.log(file_list);
                        $('#errorDiv').html(respText);
                        //console.log(xhr.responseText);
                        //console.log('COMPLETE');
                        locker.contents=render_locker(locker,file_list);
                        //console.log(locker.contents);
                        if(settings.canSelectItem && settings.selectOnUpload && locker.autoselectitem!='') {
                            selectItem(locker.autoselectitem);
                        }
                    } else {
                        var respText=xhr.responseText;
                        $('#errorDiv').html(respText);
                        alert('An error occurred!');
                    }
                };

                xhr.onerror=function() {
                    alert('ERROR-TODO');
                };

                xhr.open('GET',url,true);
                xhr.send();
            }

            function upload_files(files) {
                var file_count=files.length;
                var existing_files=[];
                var upload_file_count=0;

                //console.log(files);
                if(file_count>0) {
                    if(file_count>1 && single_item_only) {
                        //console.log('ONLY ONE FILE IS ALLOWED IN THIS LOCKER');
                        alert('Only one file is allowed in this locker.');
                    } else {    
                        var formData = new FormData();
                        
                        formData.append('path',data_path);
                        formData.append('sales_rep',settings.applicationSource);
                        formData.append('return_link','');

                        for(i=0;i<file_count;i++) {
                            var file_to_upload=files[i];
                            var filename=file_to_upload.name;
                            // console.log(filename);
                            if(existing_files.indexOf(filename)>=0) {
                                if(!confirm('The file, "'+filename+'" is already in the LogoLocker. Do you want to replace it?')) {
                                    continue;
                                }
                            }
                            // Scan filename
                            var filename_data=acceptable_filename(filename);
                            //console.log(filename_data);
                            if(!filename_data.valid) {
                                alert("The file, '"+ filename +"' contains characters that are not allowed ("+ filename_data.offending_characters_text+ "). Please rename the file without these characters.\nSuggestion: "+filename_data.suggested_filename);
                                return;
                            }
                            //console.log(file_to_upload);
                            upload_file_count++;
                            formData.append('uploaded_file[]',file_to_upload,filename);
                        }

                        if(single_item_only) {
                            //console.log('CLEARING BIN');
                        }

                        if(upload_file_count>0) {
                            //console.log('UPLOAD TO LOGOLOCKER');
                            //console.log(data_path);
                            //console.log(formData);
                            //console.log(url);

                            var progressBarWrap = $('.ll_progress_bar_wrap',locker).show();

                            var xhr = new XMLHttpRequest();
                            xhr.onload=function () {
                                if (xhr.status === 200) {
                                    var respText=xhr.responseText;
                                    $('#errorDiv').html(respText);
                                    //console.log('COMPLETE');
                                    //console.log('LOADING FILES');
                                    load_files();
                                    progressBarWrap.hide();
                                } else {
                                    var respText=xhr.responseText;
                                    $('#errorDiv').html(respText);
                                    alert('An error occurred!');
                                }
                            };
                            xhr.upload.onprogress = function(e) {
                                if (e.lengthComputable) {
                                    var percentComplete = Math.ceil((e.loaded / e.total) * 100);
                                    var progressTarget = $('[data-ll="upload-progress"]',locker);
                                    progressTarget.css('width',percentComplete+'%');
                                }
                            }

                            var url=settings.host+'/file/upload';

                            xhr.open('POST',url,true);
                            xhr.send(formData);
                        }
                    }
                }
            }

            function copy_files(source_id, destination_locker) {
                var formData = new FormData();
                formData.append('source_id',source_id);
                formData.append('destination_locker',destination_locker);

                //onsole.log(source_id);
                //console.log(destination_locker);

                //console.log(formData);

                var progressBarWrap = $('.ll_progress_bar_wrap',locker).show();

                var xhr = new XMLHttpRequest();
                xhr.onload=function () {
                    if (xhr.status === 200) {
                        var respText=xhr.responseText;
                        $('#errorDiv').html(respText);
                        //console.log('COMPLETE');
                        //console.log('LOADING FILES');
                        load_files();
                        progressBarWrap.hide();
                    } else {
                        var respText=xhr.responseText;
                        $('#errorDiv').html(respText);
                        alert('An error occurred!');
                    }
                };
                xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        var percentComplete = Math.ceil((e.loaded / e.total) * 100);
                        var progressTarget = $('[data-ll="upload-progress"]',locker);
                        progressTarget.css('width',percentComplete+'%');
                    }
                }

                var url=settings.host+'/file/copy';

                xhr.open('POST',url,true);
                xhr.send(formData);
            }

            locker.publicLink = function(){
                return querylessUri(locker.activeFile);
            }

            function querylessUri(src){
                return src;
            }

            function getQueryValue(param, url) {
                var href = url ? url : window.location.href;
                var reg = new RegExp( '[?&]' + param + '=([^&#]*)', 'i' );
                var string = reg.exec(href);
                return string ? string[1] : null;
            }

            function autoselect(files) {
                var numFiles=files.length;

                if(settings.selectOnUpload && numFiles>0) {
                    var last_file=numFiles-1;

                    var art_file=files[last_file];
                    var filename=art_file.name;

                    anchor_selector='a[href="#'+filename+'"]';

                    locker.autoselectitem=anchor_selector;
                }
            }

            function selectItem(item) {
                var anchor = $(item);

                if(settings.selected!='' && settings.replaceSelectedOnUpload==false) {
                    return;
                }
                //console.log('SELECTED');
                //console.log(anchor);
                // set active file in locker
                locker.addClass('active');
                $('[data-ll="locker-item"]>a', locker).each(function(){
                    $(this).removeClass(settings.selectedClass);
                });
                anchor.addClass(settings.selectedClass);
                // set active file link
                locker.activeFile = $('img', anchor).data('location');
                locker.activePreview = $('img', anchor).attr('src');

                //remove query data
                $(settings.targetEl).val(querylessUri(locker.activeFile));
                locker.autoselectitem='';
                settings.after.call(item, locker); // DEPRECATED
                settings.afterSelect.call(item, locker);
            }

            // Set url to target element
            $(locker).on('click', '[data-ll="locker-item"]>a', function(e){
                if(settings.canSelectItem) {
                    e.preventDefault();
                    selectItem(this);
                }
            });

            // Setup 'Remove'
            $(locker).on('click','a[data-action="remove"]',function(e) {
                e.preventDefault();
                var el=$(this);
                var href=el.attr('href');
                var location=getQueryValue('location', href);
                var thumbnail=el.closest('[data-ll="locker-item"]');
                //console.log(thumbnail);
                var isSelected=$(settings.targetEl).val().indexOf(location);
                if(isSelected>=0) {
                    alert('This image is in use. You will not be able to remove it until you select another image.')
                    return;
                }

                delete_artwork(thumbnail,location);
            });
            
            // get S3 path to store file
            var data_path=$(this).data('path') || settings.path;

            //console.log($(this).data('single'));

            // check if directory is to hold only one file
            var single_item_only=false;
            if($(this).data('single')!=undefined && $(this).data('single').toUpperCase()=='Y') {
                var single_item_only=true;
                //console.log('Single Item');
            }
            
            if(data_path!=undefined) {
                console.log('has data-path');
            } else {
                console.log('no data-path');
            }

            if(!settings.readonly) {
                // Handle drag-and-drop
                locker.on('dragstart', function(e) {
                    //console.log('dragstart');
                    //console.log(data_path);
                    //console.log(e.target);
                    e.originalEvent.dataTransfer.setData('id',$(e.target).data('id'));
                    $(this).attr('data-ll', 'dragstart');
                });
                locker.on('dragenter', function(e) {
                    //console.log('dragenter');
                    //console.log(data_path);
                    $(this).attr('data-ll', 'dragenter');
                });
                locker.on('dragover', function(e) {
                    e.preventDefault();
                    //console.log('dragover');
                    //console.log(data_path);
                    $(this).attr('data-ll', 'dragover');
                });
                locker.on('dragleave', function(e) {
                    //console.log('dragleave');
                    //console.log(data_path);
                    $(this).attr('data-ll', 'dragleave');
                });
                locker.on('drop', function(e) {
                    if(settings.readonly) {
                        return;
                    }
                    e.preventDefault();
                    e.stopPropagation();

                    //console.log('DROP');
                    //console.log(e.originalEvent.dataTransfer.getData('id'));

                    //var file_location=e.originalEvent.dataTransfer.getData('path')
                    var file_id=e.originalEvent.dataTransfer.getData('id');
                    //console.log('['+file_location+']');
                    //console.log(data_path);

                    var files=e.originalEvent.dataTransfer.files;

                    if(files.length>0) {
                        upload_files(files);
                        autoselect(files);
                    } else {
                        if(file_id=='' || file_id==undefined) {
                            alert('You cannot use this item. Please select an image from another locker or your computer.');
                            //console.log('Not allowed');
                            return;
                        } else {
                            copy_files(file_id,data_path);
                            //console.log('COPY FILES from File '+file_id+' to '+data_path);
                        }
                    }

                    $(this).attr('data-ll', 'drop');
                    settings.afterUpload.call(this, locker);
                });
                locker.on('dragend', function(e) {
                    //console.log('dragend');
                    //console.log(data_path);
                    $(this).attr('data-ll', 'dragend');
                });

                locker.on('click','[type="submit"]',function(e) {
                    e.preventDefault();
                    file_list_from_browse=$('[data-ll="fileinput"]',locker)[0];  // get underlying DOM object so we can get filelist
                    //console.log(file_list_from_browse.files);
                    upload_files(file_list_from_browse.files);
                    autoselect(file_list_from_browse.files);
                    //alert('Browse Submit');
                    settings.afterUpload.call(this, locker);
                });

                locker.on('change','[data-ll="locker-item-add"] [data-ll="fileinput"]',function(e) {
                    file_list_from_browse=$(this)[0];  // get underlying DOM object so we can get filelist
                    upload_files(file_list_from_browse.files);
                    settings.afterUpload.call(this, locker);
                });

                /* File upload button */
                /* http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3 */
                locker.on('fileselect', '.btn-file :file', function(event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text');
                    var value = numFiles > 1 ? numFiles + ' files selected' : label;
                    if(input.length) {
                        input.val(value);
                    }
                });
            }

            switch(settings.action.toLowerCase()) {
                case 'get_files':
                    //console.log('GET FILES');
                    //console.log(data_path);

                    load_files();
                    break;
                default:
            }
        });

        return this;
    }

}(jQuery));